package com.choza.victor.shoppinglist.model;

import android.util.Log;

/**
 * Created by Victor on 05/03/2018.
 */

public class ShoppingListProductPurchased {
    private int shoppingListProductPurchasedId;
    private int shoppingListId;
    private int suggestedShoppingListId;
    private int productId;
    private int purchaseId;
    private int shoppingListProductPurchasedQuantity;

    public ShoppingListProductPurchased(int shoppingListProductPurchasedId, int shoppingListId, int suggestedShoppingListId, int productId, int purchaseId, int shoppingListProductPurchasedQuantity) {
        this.shoppingListProductPurchasedId = shoppingListProductPurchasedId;
        this.shoppingListId = shoppingListId;
        this.suggestedShoppingListId = suggestedShoppingListId;
        this.productId = productId;
        this.purchaseId = purchaseId;
        this.shoppingListProductPurchasedQuantity = shoppingListProductPurchasedQuantity;
    }

    public int getshoppingListProductPurchasedId() {
        return shoppingListProductPurchasedId;
    }

    public void setshoppingListProductPurchasedId(int shoppingListProductPurchasedId) {
        this.shoppingListProductPurchasedId = shoppingListProductPurchasedId;
    }

    public int getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(int shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public int getSuggestedShoppingListId() {
        return suggestedShoppingListId;
    }

    public void setSuggestedShoppingListId(int suggestedShoppingListId) {
        this.suggestedShoppingListId = suggestedShoppingListId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShoppingListProductPurchasedId() {
        return shoppingListProductPurchasedId;
    }

    public void setShoppingListProductPurchasedId(int shoppingListProductPurchasedId) {
        this.shoppingListProductPurchasedId = shoppingListProductPurchasedId;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getShoppingListProductPurchasedQuantity() {
        return shoppingListProductPurchasedQuantity;
    }

    public void setShoppingListProductPurchasedQuantity(int shoppingListProductPurchasedQuantity) {
        this.shoppingListProductPurchasedQuantity = shoppingListProductPurchasedQuantity;
    }
}
