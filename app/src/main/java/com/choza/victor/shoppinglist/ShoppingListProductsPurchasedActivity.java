package com.choza.victor.shoppinglist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.choza.victor.shoppinglist.adapter.ListProductsPurchasedAdapter;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.ShoppingList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class ShoppingListProductsPurchasedActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ListView listViewProducts;
    public ListProductsPurchasedAdapter listProductsPurchasedAdapter;
    private List<Product> products;
    private ShoppingList shoppingList;
    private DatabaseHelper databaseHelper;
    private int shoppingListID;
    private boolean dataChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list_purchased);

        dataChanged = false;

        toolbar = (Toolbar) findViewById(R.id.activity_shopping_list_purchased_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        Intent intentData = getIntent();
        if (intentData != null) {
            shoppingListID = intentData.getIntExtra("shoppingListID", 0);
        }

        listViewProducts = (ListView) this.findViewById(R.id.list_products_purchased);
        databaseHelper = new DatabaseHelper(this);


        updateList();

        this.setTitle("Checked Products");
    }

    @Override
    public void onBackPressed() {
        Intent resInfo = new Intent();
        if (dataChanged) {
            resInfo.putExtra("activity", "ShoppingInfo");
            setResult(RESULT_OK, resInfo);
        }

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent resInfo = new Intent();
                if (dataChanged) {
                    resInfo.putExtra("activity", "ShoppingInfo");
                    setResult(RESULT_OK, resInfo);
                }

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean copyDatabase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void updateList() {
        //Check exists database
        final File database = this.getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if (!database.exists()) {
            databaseHelper.getReadableDatabase();
            //Copy db
            if (copyDatabase(this)) {
                Toast.makeText(this, R.string.copy_database_success, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.copy_database_error, Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //Get product list in db when db exists
        products = databaseHelper.getListProductsPurchasedByShoppingList(shoppingListID);
        shoppingList = databaseHelper.getShoppingListById(shoppingListID);
        //Init adapter
        listProductsPurchasedAdapter = new ListProductsPurchasedAdapter(this, products, shoppingList);
        //Set adapter for listview
        listViewProducts.setAdapter(listProductsPurchasedAdapter);
        listViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
                products = databaseHelper.getListProductsPurchasedByShoppingList(shoppingListID);
                AlertDialog.Builder dialogDelete = new AlertDialog.Builder(ShoppingListProductsPurchasedActivity.this);

                dialogDelete.setTitle(R.string.unckeck_product);
                dialogDelete.setMessage(R.string.confirmation_uncheck_product);
                dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        databaseHelper.addShoppingListProduct(shoppingListID, products.get(position).getProductID(), databaseHelper.getShoppingListProductPurchasedByShoppingListIdAndProductId(shoppingListID, products.get(position).getProductID()).getShoppingListProductPurchasedQuantity());
                        databaseHelper.deletePurchaseById(databaseHelper.getShoppingListProductPurchasedByShoppingListIdAndProductId(shoppingListID, products.get(position).getProductID()).getPurchaseId());
                        databaseHelper.updateProductLastPurchase(products.get(position).getProductID());
                        databaseHelper.deleteProductFromShoppingListPurchasedByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingListID);
                        listProductsPurchasedAdapter.updateList(databaseHelper.getListProductsPurchasedByShoppingList(shoppingListID));
                        dataChanged = true;
                    }
                });


                dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogDelete.show();
            }
        });
        /*listViewProducts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, final long id) {
                products = databaseHelper.getListProductsPurchasedByShoppingList(shoppingListID);
                AlertDialog.Builder dialogDelete = new AlertDialog.Builder(ShoppingListProductsPurchasedActivity.this);

                dialogDelete.setTitle(R.string.unckeck_product);
                dialogDelete.setMessage(R.string.confirmation_uncheck_product);
                dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        databaseHelper.addShoppingListProduct(shoppingListID, products.get(position).getProductID(), databaseHelper.getShoppingListProductPurchasedByShoppingListIdAndProductId(shoppingListID, products.get(position).getProductID()).getShoppingListProductPurchasedQuantity());
                        databaseHelper.deletePurchaseById(databaseHelper.getShoppingListProductPurchasedByShoppingListIdAndProductId(shoppingListID, products.get(position).getProductID()).getPurchaseId());
                        databaseHelper.updateProductLastPurchase(products.get(position).getProductID());
                        databaseHelper.deleteProductFromShoppingListPurchasedByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingListID);
                        listProductsPurchasedAdapter.updateList(databaseHelper.getListProductsPurchasedByShoppingList(shoppingListID));
                        dataChanged = true;
                    }
                });


                dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogDelete.show();

                return false;
            }
        });*/

        registerForContextMenu(listViewProducts);
    }
}
