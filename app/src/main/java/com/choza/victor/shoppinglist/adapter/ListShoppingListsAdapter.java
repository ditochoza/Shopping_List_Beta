package com.choza.victor.shoppinglist.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.choza.victor.shoppinglist.Fragment_Home;
import com.choza.victor.shoppinglist.InfoShoppingListActivity;
import com.choza.victor.shoppinglist.MainActivity;
import com.choza.victor.shoppinglist.R;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.ShoppingList;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Victor on 04/03/2018.
 */

public class ListShoppingListsAdapter extends RecyclerView.Adapter<ListShoppingListsAdapter.ViewHolderShoppingLists> {
    private Context context;
    private List<ShoppingList> shoppingLists;
    private DatabaseHelper databaseHelper;
    private int INFO_SHOPPING_LIST = 3;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolderShoppingLists extends RecyclerView.ViewHolder {
        public CardView cardView;
        public TextView nameShoppingListTextView;
        public TextView productsShoppingListTextView;
        public Button deleteShoppingList, markAsDoneShoppingList;

        ViewHolderShoppingLists(View view){
            super(view);
            cardView = (CardView) view.findViewById(R.id.item_listview_products_shopping_list_card_view);
            nameShoppingListTextView = (TextView) view.findViewById(R.id.nameShoppingListTextView);
            productsShoppingListTextView = (TextView) view.findViewById(R.id.productsShoppingListTextView);
            deleteShoppingList = (Button) view.findViewById(R.id.buttonDeleteShoppingList);
            markAsDoneShoppingList = (Button) view.findViewById(R.id.buttonMarkAsDoneShoppingList);
            final View v = view;
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentInfo = new Intent(context, InfoShoppingListActivity.class);
                    intentInfo.putExtra("shoppingListID", shoppingLists.get(getAdapterPosition()).getShoppingListId());

                    ((MainActivity)context).startActivityForResult(intentInfo, INFO_SHOPPING_LIST);
                }
            });

            deleteShoppingList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

                    dialogDelete.setTitle(R.string.delete_shopping_list);
                    dialogDelete.setMessage(R.string.confirmation_delete_shopping_list);
                    dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            databaseHelper.deleteShoppingListProductPurchasedByShoppingListId(shoppingLists.get(getAdapterPosition()).getShoppingListId());
                            databaseHelper.deleteShoppingListProductByShoppingListId(shoppingLists.get(getAdapterPosition()).getShoppingListId());
                            databaseHelper.deleteShoppingListById(shoppingLists.get(getAdapterPosition()).getShoppingListId());

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    shoppingLists.remove(getAdapterPosition());
                                    notifyDataSetChanged();
                                }
                            }, 200);
                            Toast.makeText(context, R.string.deleting, Toast.LENGTH_SHORT).show();
                        }
                    });


                    dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialogDelete.show();
                }
            });

            markAsDoneShoppingList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

                    dialogDelete.setTitle(R.string.mark_as_completed);
                    dialogDelete.setMessage(R.string.confirmation_mark_as_completed);
                    dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            String currentDate = dateFormat.format(Calendar.getInstance().getTime());

                            List<Product> products = databaseHelper.getListProductsByShoppingListId(shoppingLists.get(getAdapterPosition()).getShoppingListId());
                            for (int i = 0; i < products.size(); i++){
                                databaseHelper.addPurchase(products.get(i).getProductID(), currentDate, databaseHelper.getShoppingListProductByProductIdAndShoppingListId(products.get(i).getProductID(), shoppingLists.get(getAdapterPosition()).getShoppingListId()).getShoppingListProductQuantity());
                                databaseHelper.updateProductLastPurchase(products.get(i).getProductID());
                            }
                            databaseHelper.deleteShoppingListProductPurchasedByShoppingListId(shoppingLists.get(getAdapterPosition()).getShoppingListId());
                            databaseHelper.deleteShoppingListProductByShoppingListId(shoppingLists.get(getAdapterPosition()).getShoppingListId());
                            databaseHelper.deleteShoppingListById(shoppingLists.get(getAdapterPosition()).getShoppingListId());

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    shoppingLists.remove(getAdapterPosition());
                                    notifyDataSetChanged();
                                }
                            }, 200);
                            Toast.makeText(context, R.string.deleting, Toast.LENGTH_SHORT).show();
                        }
                    });


                    dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialogDelete.show();
                }
            });

        }
    }

    public ListShoppingListsAdapter(Fragment_Home fragment_home, Context context, List<ShoppingList> shoppingLists) {
        this.context = context;
        this.shoppingLists = shoppingLists;
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public ViewHolderShoppingLists onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listview_shoppinglists, parent, false);

        return new ViewHolderShoppingLists(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderShoppingLists viewHolderShoppingLists, int position) {
        List<Product> products = databaseHelper.getListProductsByShoppingListId(shoppingLists.get(position).getShoppingListId());

        viewHolderShoppingLists.nameShoppingListTextView.setText(shoppingLists.get(position).getShoppingListName());
        String productNames = "";
        for (int i = 0; i < products.size(); i++){
            if (i != 0){
                productNames += "\n";
            }
            productNames += products.get(i).getProductName();

            String cheapestPrice = products.get(i).getProductCheapestPrice();
            if (cheapestPrice != null){
                productNames += ": " + cheapestPrice;
            }
        }
        viewHolderShoppingLists.productsShoppingListTextView.setText(productNames);
    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public void updateList(List<ShoppingList> lstItem) {
        shoppingLists.clear();
        shoppingLists.addAll(lstItem);
        this.notifyDataSetChanged();
    }
}
