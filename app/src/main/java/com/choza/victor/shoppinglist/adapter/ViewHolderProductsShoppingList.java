package com.choza.victor.shoppinglist.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.choza.victor.shoppinglist.R;

/**
 * Created by Victor on 04/03/2018.
 */

public class ViewHolderProductsShoppingList {
    public TextView nameProductTextView, priceProductTextView, quantityProductTextView;
    public ImageButton deleteProduct, checkProduct;
    public RelativeLayout relativeLayout;

    ViewHolderProductsShoppingList(View view){
        nameProductTextView = (TextView) view.findViewById(R.id.name_product_shopping_list);
        priceProductTextView = (TextView) view.findViewById(R.id.price_product_shopping_list);
        quantityProductTextView = (TextView) view.findViewById(R.id.quantity_product_shopping_list);
        checkProduct = (ImageButton) view.findViewById(R.id.checkProduct);
        deleteProduct = (ImageButton) view.findViewById(R.id.deleteProduct);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.item_listview_products_shopping_list_relative_layout);

    }
}
