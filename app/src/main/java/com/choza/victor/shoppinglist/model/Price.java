package com.choza.victor.shoppinglist.model;

/**
 * Created by Victor on 03/03/2018.
 */

public class Price {
    private int priceID;
    private double priceTag;
    private String priceDate;
    private int supermarketID;
    private int productID;

    public Price(int priceID, double priceTag, String priceDate, int supermarketID, int productID) {
        this.priceID = priceID;
        this.priceTag = priceTag;
        this.priceDate = priceDate;
        this.supermarketID = supermarketID;
        this.productID = productID;
    }

    public int getPriceID() {
        return priceID;
    }

    public void setPriceID(int priceID) {
        this.priceID = priceID;
    }

    public double getPriceTag() {
        return priceTag;
    }

    public void setPriceTag(double priceTag) {
        this.priceTag = priceTag;
    }

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public int getSupermarketID() {
        return supermarketID;
    }

    public void setSupermarketID(int supermarketID) {
        this.supermarketID = supermarketID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }
}
