package com.choza.victor.shoppinglist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.choza.victor.shoppinglist.adapter.ListProductsShoppingListAdapter;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.ShoppingList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class InfoShoppingListActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private AutoCompleteTextView productText;
    private TextView nameText;
    private int shoppingListID;
    private FabSpeedDial fabSpeedDial;
    private ListView listViewProductsShoppingList;
    private DatabaseHelper databaseHelper;
    private ImageButton addProduct;
    private ListProductsShoppingListAdapter listProductShoppingListAdapter;
    private List<Product> products;
    private boolean scrollToBottom = false;
    private Spinner spinner;
    private ShoppingList shoppingList;
    private int PRODUCTS_PURCHASED_ACTIVITY = 8;
    public boolean dataChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_shopping_list);

        dataChanged = false;

        Intent intentData = getIntent();
        if (intentData != null) {
            shoppingListID = intentData.getIntExtra("shoppingListID", 0);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbarInfoShoppingList);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        listViewProductsShoppingList = (ListView) findViewById(R.id.list_productos_shopping_list);
        nameText = (TextView) findViewById(R.id.shoppingListEditNameTextView);
        productText = (AutoCompleteTextView) findViewById(R.id.shoppingListProductEditNameTextView);
        addProduct = (ImageButton) findViewById(R.id.addProductImage);
        spinner = (Spinner) findViewById(R.id.spinner);
        databaseHelper = new DatabaseHelper(this);

        String[] numbersString = new String[50];
        for (int i = 0; i < 50; i++) {
            numbersString[i] = String.valueOf((i + 1));
        }

        ArrayAdapter<String> adapterNumbers = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, numbersString);
        adapterNumbers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterNumbers);

        String[] productsString = databaseHelper.getListProductNames().toArray(new String[0]);
        ArrayAdapter<String> adapterProducts = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, productsString);
        productText.setAdapter(adapterProducts);

        productText.requestFocus();

        registerForContextMenu(listViewProductsShoppingList);

        fabSpeedDial = (FabSpeedDial) findViewById(R.id.fabShoppingList);

        updateFields();
        updateList();
        setListeners();

        setTitle(getString(R.string.shopping_list));
    }

    private void updateList() {
        //buttonAdd = (Button)getActivity().findViewById(R.id.fab);
        //Check exists database
        File database = this.getDatabasePath(DatabaseHelper.DBNAME);
        if (!database.exists()) {
            databaseHelper.getReadableDatabase();
            //Copy db
            if (copyDatabase(this)) {
                Toast.makeText(this, R.string.copy_database_success, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.copy_database_error, Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //Get product list in db when db exists
        products = databaseHelper.getListProductsByShoppingListId(shoppingListID);
        shoppingList = databaseHelper.getShoppingListById(shoppingListID);
        //Init adapter
        listProductShoppingListAdapter = new ListProductsShoppingListAdapter(this, products, shoppingList, InfoShoppingListActivity.this);
        //Set adapter for listview
        listViewProductsShoppingList.setAdapter(listProductShoppingListAdapter);

        if (scrollToBottom) {
            listViewProductsShoppingList.setSelection(listViewProductsShoppingList.getCount() - 1);
            scrollToBottom = false;
        }
    }

    @Override
    public void onBackPressed() {
        Intent resInfo = new Intent();
        if (dataChanged) {
            resInfo.putExtra("activity", "Shopping");
            setResult(RESULT_OK, resInfo);
        }

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent resInfo = new Intent();
                if (dataChanged) {
                    resInfo.putExtra("activity", "Shopping");
                    setResult(RESULT_OK, resInfo);
                }

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean copyDatabase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void updateFields() {
        nameText.setText(databaseHelper.getShoppingListById(shoppingListID).getShoppingListName());
    }

    private void setListeners() {
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.fab_menu_info_shopping_list_edit) {
                    //Se crea el cuadro de dialogo y se le pone un título
                    final AlertDialog.Builder builder = new AlertDialog.Builder(InfoShoppingListActivity.this);
                    builder.setTitle(R.string.edit_shopping_list_name);

                    //Se usa el inflate con la vista que hemos creado
                    View viewInflated = LayoutInflater.from(InfoShoppingListActivity.this).inflate(R.layout.dialog_edit_shopping_list, null, false);

                    //Se configura el TextInput
                    final TextInputEditText inputName = (TextInputEditText) viewInflated.findViewById(R.id.dialog_edit_shopping_list_name);

                    //Se le administra la vista creada al dialog
                    builder.setView(viewInflated);

                    //Se configura el listener del botón aceptar (Actualizar nombre producto)
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateShoppingList();
                            dataChanged = true;
                            dialog.dismiss();
                        }

                        private void updateShoppingList() {
                            //Se comprueba si el campo de texto está vacío y si el nombre ya existe
                            if (!String.valueOf(inputName.getText()).isEmpty()) {
                                boolean shoppingListExists = false;
                                List<ShoppingList> shoppingLists = databaseHelper.getListShoppingLists();
                                for (int i = 0; i < shoppingLists.size(); i++) {
                                    if (shoppingLists.get(i).getShoppingListName().toLowerCase().equals(String.valueOf(inputName.getText()).toLowerCase())) {
                                        shoppingListExists = true;
                                    }
                                }

                                if (!shoppingListExists) {
                                    databaseHelper.updateShoppingList(String.valueOf(inputName.getText()), shoppingListID);
                                    updateFields();
                                    Toast.makeText(getApplicationContext(), R.string.saving, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), R.string.name_unique, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                    //Se configura el listener del botón cancelar
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                    return true;
                } else if (menuItem.getItemId() == R.id.fab_menu_info_shopping_list_checked_products) {
                    Intent intentInfo = new Intent(InfoShoppingListActivity.this, ShoppingListProductsPurchasedActivity.class);
                    intentInfo.putExtra("shoppingListID", shoppingList.getShoppingListId());

                    InfoShoppingListActivity.this.startActivityForResult(intentInfo, PRODUCTS_PURCHASED_ACTIVITY);
                    return true;
                } else if (menuItem.getItemId() == R.id.fab_menu_info_shopping_list_delete) {
                    AlertDialog.Builder dialogDelete = new AlertDialog.Builder(InfoShoppingListActivity.this);

                    dialogDelete.setTitle(R.string.delete_shopping_list);
                    dialogDelete.setMessage(R.string.confirmation_delete_shopping_list_3);
                    dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            databaseHelper.deleteShoppingListProductPurchasedByShoppingListId(shoppingListID);
                            databaseHelper.deleteShoppingListProductByShoppingListId(shoppingListID);
                            databaseHelper.deleteShoppingListById(shoppingListID);

                            Intent resInfo = new Intent();
                            resInfo.putExtra("activity", "Shopping");
                            setResult(RESULT_OK, resInfo);

                            finish();
                            Toast.makeText(getApplicationContext(), R.string.deleting, Toast.LENGTH_SHORT).show();
                        }
                    });


                    dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialogDelete.show();
                    return true;
                } else {
                    return false;
                }
            }
        });

        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!String.valueOf(productText.getText()).equals("")) {
                    boolean exists = false;
                    List<Product> products = databaseHelper.getListProducts();
                    for (int i = 0; i < products.size(); i++) {
                        if (products.get(i).getProductName().toLowerCase().equals(String.valueOf(productText.getText()).toLowerCase())) {
                            exists = true;
                        }
                    }

                    if (!exists) {
                        databaseHelper.addProduct(String.valueOf(productText.getText()));
                        databaseHelper.addShoppingListProduct(shoppingListID, databaseHelper.getProductByName(String.valueOf(productText.getText())).getProductID(), Integer.parseInt(spinner.getSelectedItem().toString()));
                        productText.setText("");
                        spinner.setSelection(0);
                        dataChanged = true;
                        scrollToBottom = true;
                        updateList();
                        Toast.makeText(getApplicationContext(), R.string.new_product_created_saving, Toast.LENGTH_SHORT).show();
                    } else {
                        exists = false;
                        for (int i = 0; i < products.size(); i++) {
                            if (products.get(i).getProductName().equals(String.valueOf(productText.getText()))) {
                                exists = true;
                            }
                        }
                        if (exists) {
                            products = databaseHelper.getListProductsByShoppingListId(shoppingListID);
                            exists = false;
                            for (int i = 0; i < products.size(); i++) {
                                if (products.get(i).getProductName().toLowerCase().equals(String.valueOf(productText.getText()).toLowerCase())) {
                                    exists = true;
                                }
                            }
                            products = databaseHelper.getListProductsPurchasedByShoppingList(shoppingListID);
                            for (int i = 0; i < products.size(); i++) {
                                if (products.get(i).getProductName().toLowerCase().equals(String.valueOf(productText.getText()).toLowerCase())) {
                                    exists = true;
                                }
                            }

                            if (!exists) {
                                databaseHelper.addShoppingListProduct(shoppingListID, databaseHelper.getProductByName(String.valueOf(productText.getText())).getProductID(), Integer.parseInt(spinner.getSelectedItem().toString()));
                                productText.setText("");
                                spinner.setSelection(0);
                                dataChanged = true;
                                scrollToBottom = true;
                                updateList();
                                Toast.makeText(getApplicationContext(), R.string.saving, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.product_exists, Toast.LENGTH_SHORT).show();
                                productText.setText("");
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.name_written_incorrectly, Toast.LENGTH_SHORT).show();
                            productText.setText("");
                        }

                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                    productText.setText("");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data.getStringExtra("activity").equals("ShoppingInfo")) {
                dataChanged = true;
                listProductShoppingListAdapter.updateList(databaseHelper.getListProductsByShoppingListId(shoppingListID));
            }
        }
    }
}
