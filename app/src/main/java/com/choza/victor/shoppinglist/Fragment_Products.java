package com.choza.victor.shoppinglist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.choza.victor.shoppinglist.adapter.ListProductsAdapter;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.Supermarket;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by Victor on 03/03/2018.
 */

public class Fragment_Products extends Fragment {
    private ListView listViewProducts;
    public ListProductsAdapter listProductsAdapter;
    private List<Product> products;
    private DatabaseHelper databaseHelper;
    private FloatingActionButton fab;
    private static final int INFO_PRODUCT_ACTIVITY = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_products, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listViewProducts = (ListView) getActivity().findViewById(R.id.list_products);
        databaseHelper = new DatabaseHelper(getActivity());

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fabMain);


        updateList();
        setListeners();

        getActivity().setTitle(getString(R.string.products));
    }

    private boolean copyDatabase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setListeners() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //Se crea el cuadro de dialogo y se le pone un título
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.new_product);

                //Se usa el inflate con la vista que hemos creado
                View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog_add_product, (ViewGroup) getView(), false);

                //Se configura el TextInput
                final TextInputEditText input = (TextInputEditText) viewInflated.findViewById(R.id.dialog_add_product_name);

                //Se le administra la vista creada al dialog
                builder.setView(viewInflated);

                //Se configura el listener del botón aceptar (Actualizar nombre producto)
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addProduct();
                        dialog.dismiss();
                    }

                    private void addProduct() {
                        if (!String.valueOf(input.getText()).isEmpty()) {
                            boolean productExtists = false;
                            for (int i = 0; i < products.size(); i++) {
                                if (products.get(i).getProductName().toLowerCase().equals(String.valueOf(input.getText()).toLowerCase())) {
                                    productExtists = true;
                                }
                            }
                            if (!productExtists) {
                                databaseHelper.addProduct(String.valueOf(input.getText()));
                                listProductsAdapter.updateList(databaseHelper.getListProducts());

                                Toast.makeText(getActivity().getApplicationContext(), databaseHelper.getListProducts().get(listProductsAdapter.getCount()-1).getProductName() + " " + getString(R.string.created), Toast.LENGTH_SHORT).show();

                                Intent intentInfo = new Intent(getActivity(), InfoProductActivity.class);
                                intentInfo.putExtra("productID", databaseHelper.getListProducts().get(listProductsAdapter.getCount()-1).getProductID());

                                startActivityForResult(intentInfo, INFO_PRODUCT_ACTIVITY);
                            } else {
                                Toast.makeText(getActivity(), R.string.name_unique, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                //Se configura el listener del botón cancelar
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    private void updateList() {
        //Check exists database
        File database = getActivity().getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if (!database.exists()) {
            databaseHelper.getReadableDatabase();
            //Copy db
            if (copyDatabase(getActivity())) {
                Toast.makeText(getActivity(), "Copy database success", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Copy data error", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //Get product list in db when db exists
        products = databaseHelper.getListProducts();
        //Init adapter
        listProductsAdapter = new ListProductsAdapter(getActivity(), products);
        //Set adapter for listview
        listViewProducts.setAdapter(listProductsAdapter);
        listViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity().getApplicationContext(), ((TextView) view.findViewById(R.id.name_product)).getText().toString(), Toast.LENGTH_SHORT).show();
                Intent intentInfo = new Intent(getActivity(), InfoProductActivity.class);
                intentInfo.putExtra("productID", databaseHelper.getListProducts().get(position).getProductID());

                startActivityForResult(intentInfo, INFO_PRODUCT_ACTIVITY);

            }
        });

        registerForContextMenu(listViewProducts);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data.getStringExtra("activity").equals("Info")) {
                listProductsAdapter.updateList(databaseHelper.getListProducts());
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu_products_list, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //Get id of item clicked
        //Retrieve the item that was clicked on
        View view = (View) listViewProducts.getAdapter().getView(info.position, null, null);
        final Product product = (Product) listViewProducts.getItemAtPosition(info.position);
        final View viewInflated;
        final AlertDialog.Builder builder;

        switch (item.getItemId()) {
            case R.id.context_menu_products_list_edit:
                //Se crea el cuadro de dialogo y se le pone un título
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Edit Product Name");

                //Se usa el inflate con la vista que hemos creado
                viewInflated = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_product, null, false);

                //Se configura el TextInput
                final TextInputEditText inputProductName = (TextInputEditText) viewInflated.findViewById(R.id.dialog_edit_product_name);

                //Se le administra la vista creada al dialog
                builder.setView(viewInflated);

                //Se configura el listener del botón aceptar (Actualizar nombre producto)
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateProduct();
                        listProductsAdapter.updateList(databaseHelper.getListProducts());
                        dialog.dismiss();
                    }

                    private void updateProduct() {
                        //Se comprueba si el campo de texto está vacío y si el nombre ya existe
                        if (!String.valueOf(inputProductName.getText()).isEmpty()) {
                            boolean productExtists = false;
                            List<Product> products = databaseHelper.getListProducts();
                            for (int i = 0; i < products.size(); i++) {
                                if (products.get(i).getProductName().toLowerCase().equals(String.valueOf(inputProductName.getText()).toLowerCase())) {
                                    productExtists = true;
                                }
                            }

                            if (!productExtists) {
                                databaseHelper.updateProduct(String.valueOf(inputProductName.getText()), product.getProductID());
                                Toast.makeText(getActivity(), R.string.saving, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.name_unique, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                        }

                    }
                });

                //Se configura el listener del botón cancelar
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case R.id.context_menu_products_list_add_price:
                //Se crea el cuadro de dialogo y se le pone un título
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("New Price");

                //Se usa el inflate con la vista que hemos creado
                 viewInflated = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_price, null, false);

                //Se configuran los TextInputs
                final AutoCompleteTextView inputSupermarketName = (AutoCompleteTextView) viewInflated.findViewById(R.id.dialog_add_price_name);
                final TextInputEditText inputPrice = (TextInputEditText) viewInflated.findViewById(R.id.dialog_add_price_price);

                //Se cargan las opciones del AutoCompleteTextView
                String[] supermarketsString = databaseHelper.getListSupermarketNames().toArray(new String[0]);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, supermarketsString);
                inputSupermarketName.setAdapter(adapter);

                //Se le administra la vista creada al dialog
                builder.setView(viewInflated);

                //Se configura el listener del botón aceptar (Añadir precio)
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addPrice();
                        listProductsAdapter.updateList(databaseHelper.getListProducts());
                        dialog.dismiss();
                    }

                    private void addPrice() {

                        //Se comprueba que los campos de texto no estén vacíos
                        //Si no existe el supermercado se crea
                        if (!String.valueOf(inputSupermarketName.getText()).isEmpty()) {
                            boolean productExtists = false;
                            List<Supermarket> supermarkets = databaseHelper.getListSupermarkets();
                            for (int i = 0; i < supermarkets.size(); i++) {
                                if (supermarkets.get(i).getSupermarketName().toLowerCase().equals(String.valueOf(inputSupermarketName.getText()).toLowerCase())) {
                                    productExtists = true;
                                }
                            }

                            if (!productExtists) {
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                String currentDate = dateFormat.format(Calendar.getInstance().getTime());

                                databaseHelper.addSupermarket(String.valueOf(inputSupermarketName.getText()));
                                databaseHelper.addPrice(databaseHelper.getSupermarketIdBySupermarketName(String.valueOf(inputSupermarketName.getText())), Double.parseDouble(String.valueOf(inputPrice.getText())), currentDate, product.getProductID());
                                databaseHelper.updateProductCheapestPrice(product.getProductID());
                                listProductsAdapter.notifyDataSetChanged();
                            } else {
                                productExtists = false;
                                for (int i = 0; i < supermarkets.size(); i++) {
                                    if (supermarkets.get(i).getSupermarketName().equals(String.valueOf(inputSupermarketName.getText()))) {
                                        productExtists = true;
                                    }
                                }

                                if (productExtists) {
                                    @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    String currentDate = dateFormat.format(Calendar.getInstance().getTime());

                                    databaseHelper.addPrice(databaseHelper.getSupermarketIdBySupermarketName(String.valueOf(inputSupermarketName.getText())), Double.parseDouble(String.valueOf(inputPrice.getText())), currentDate, product.getProductID());
                                    databaseHelper.updateProductCheapestPrice(product.getProductID());

                                    Toast.makeText(getActivity(), R.string.saving, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.name_written_incorrectly, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                //Se configura el listener del botón cancelar
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case R.id.context_menu_products_list_delete:
                AlertDialog.Builder dialogDelete = new AlertDialog.Builder(getActivity());

                dialogDelete.setTitle(R.string.delete_product);
                dialogDelete.setMessage(R.string.confirmation_delete_product);
                dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        databaseHelper.deleteProductFromShoppingListByProductId(product.getProductID());
                        databaseHelper.deteleProductById(product.getProductID());

                        listProductsAdapter.updateList(databaseHelper.getListProducts());

                        Toast.makeText(getActivity(), R.string.deleting, Toast.LENGTH_SHORT).show();
                    }
                });


                dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogDelete.show();
                break;
            default:
                return false;
        }
        return true;
    }
}
