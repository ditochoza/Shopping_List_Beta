package com.choza.victor.shoppinglist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.choza.victor.shoppinglist.adapter.ListShoppingListsAdapter;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.ShoppingList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Victor on 03/03/2018.
 */

public class Fragment_Home extends Fragment {

    //private ListView listViewCurrent, listViewUpcoming, listViewSuggested;
    private RecyclerView listViewCurrent;
    private ListShoppingListsAdapter listShoppingListsAdapter;
    private DatabaseHelper databaseHelper;
    private List<ShoppingList> shoppingLists;
    private FloatingActionButton fab;
    private Button deleteButton, completeButton;
    private int INFO_SHOPPING_LIST_ACTIVITY = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fabMain);


        listViewCurrent = (RecyclerView) getActivity().findViewById(R.id.list_home_current);
        //listViewUpcoming = (ListView)getActivity().findViewById(R.id.list_home_upcoming);
        //listViewSuggested = (ListView)getActivity().findViewById(R.id.list_home_suggested);

        listViewCurrent.setLayoutManager(new LinearLayoutManager(getActivity()));

        databaseHelper = new DatabaseHelper(getActivity());

        updateList();
        setListeners();

        getActivity().setTitle(getString(R.string.shopping_lists));
    }

    private void updateList() {
        //Check exists database
        File database = getActivity().getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if (!database.exists()) {
            databaseHelper.getReadableDatabase();
            //Copy db
            if (copyDatabase(getActivity())) {
                Toast.makeText(getActivity(), R.string.copy_database_success, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.copy_database_error, Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //Get product list in db when db exists
        shoppingLists = databaseHelper.getListShoppingLists();
        //Init adapter
        listShoppingListsAdapter = new ListShoppingListsAdapter(this, getActivity(), shoppingLists);
        //Set adapter for listview
        listViewCurrent.setAdapter(listShoppingListsAdapter);


        //ListUtils.setDynamicHeight(listViewCurrent);
        //ListUtils.setDynamicHeight(listViewUpcoming);
        //ListUtils.setDynamicHeight(listViewSuggested);

        registerForContextMenu(listViewCurrent);
    }

    private boolean copyDatabase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setListeners() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Se crea el cuadro de dialogo y se le pone un título
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.new_shopping_list);

                //Se usa el inflate con la vista que hemos creado
                View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog_add_shopping_list, (ViewGroup) getView(), false);

                //Se configuran los TextInputs
                final TextInputEditText input = (TextInputEditText) viewInflated.findViewById(R.id.dialog_add_shopping_list_name);

                //Se le administra la vista creada al dialog
                builder.setView(viewInflated);

                //Se configura el listener del botón aceptar (Añadir precio)
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addShoppingList();
                        dialog.dismiss();
                    }

                    private void addShoppingList() {

                        //Se comprueba que los campos de texto no estén vacíos
                        //Se comprueba que el nombre no esté repetido
                        if (!String.valueOf(input.getText()).isEmpty()){
                            List<ShoppingList> shoppingLists = databaseHelper.getListShoppingLists();
                            boolean exists = false;
                            for (int i = 0; i < shoppingLists.size(); i++) {
                                if (shoppingLists.get(i).getShoppingListName().toLowerCase().equals(String.valueOf(input.getText()).toLowerCase())) {
                                    exists = true;
                                }
                            }

                            if (!exists){
                                databaseHelper.addShoppingList(String.valueOf(input.getText()));
                                listShoppingListsAdapter.updateList(databaseHelper.getListShoppingLists());
                                Toast.makeText(getActivity().getApplicationContext(), databaseHelper.getListShoppingLists().get(listShoppingListsAdapter.getItemCount()-1).getShoppingListName() + " " + getString(R.string.created), Toast.LENGTH_SHORT).show();

                                Intent intentInfo = new Intent(getActivity(), InfoShoppingListActivity.class);
                                intentInfo.putExtra("shoppingListID", databaseHelper.getListShoppingLists().get(listShoppingListsAdapter.getItemCount()-1).getShoppingListId());

                                startActivityForResult(intentInfo, INFO_SHOPPING_LIST_ACTIVITY);
                            }else{
                                Toast.makeText(getActivity(), R.string.name_unique, Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getActivity(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                //Se configura el listener del botón cancelar
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data.getStringExtra("activity").equals("Shopping")) {
                listShoppingListsAdapter.updateList(databaseHelper.getListShoppingLists());
            }
        }
    }
}
