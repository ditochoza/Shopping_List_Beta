package com.choza.victor.shoppinglist.model;

/**
 * Created by Victor on 03/03/2018.
 */

public class Supermarket {
    private int supermarketID;
    private String supermarketName;

    public Supermarket(int supermarketID, String supermarketName) {
        this.supermarketID = supermarketID;
        this.supermarketName = supermarketName;
    }

    public int getSupermarketID() {
        return supermarketID;
    }

    public void setSupermarketID(int supermarketID) {
        this.supermarketID = supermarketID;
    }

    public String getSupermarketName() {
        return supermarketName;
    }

    public void setSupermarketName(String supermarketName) {
        this.supermarketName = supermarketName;
    }
}
