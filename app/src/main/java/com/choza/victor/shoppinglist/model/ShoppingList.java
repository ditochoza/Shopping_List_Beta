package com.choza.victor.shoppinglist.model;

/**
 * Created by Victor on 04/03/2018.
 */

public class ShoppingList {
    private int shoppingListId;
    private String shoppingListName;

    public ShoppingList(int shoppingListId, String shoppingListName) {
        this.shoppingListId = shoppingListId;
        this.shoppingListName = shoppingListName;
    }

    public int getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(int shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public String getShoppingListName() {
        return shoppingListName;
    }

    public void setShoppingListName(String shoppingListName) {
        this.shoppingListName = shoppingListName;
    }
}
