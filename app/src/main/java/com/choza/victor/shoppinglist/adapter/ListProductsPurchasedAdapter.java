package com.choza.victor.shoppinglist.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.choza.victor.shoppinglist.R;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.ShoppingList;

import java.util.List;

/**
 * Created by Victor on 09/03/2018.
 */

public class ListProductsPurchasedAdapter extends BaseAdapter{
    private Context context;
    private List<Product> products;
    private LayoutInflater inflater;
    private DatabaseHelper databaseHelper;
    private ShoppingList shoppingList;

    public ListProductsPurchasedAdapter(Context context, List<Product> products, ShoppingList shoppingList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.products = products;
        this.shoppingList = shoppingList;
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getProductID();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {

        View layout = convertView;
        final ViewHolderProductsPurchased viewHolderProductsPurchased;
        final Product product = products.get(position);

        if (layout == null) {
            layout = inflater.inflate(R.layout.item_listview_products_purchased, parent, false);
            viewHolderProductsPurchased = new ViewHolderProductsPurchased(layout);
            layout.setTag(viewHolderProductsPurchased);
        } else {
            viewHolderProductsPurchased = (ViewHolderProductsPurchased) layout.getTag();
        }

        viewHolderProductsPurchased.nameProductTextView.setText(products.get(position).getProductName());

        String cheapestPrice = product.getProductCheapestPrice();
        if (cheapestPrice == null){
            viewHolderProductsPurchased.priceProductTextView.setText("");
        }else {
            viewHolderProductsPurchased.priceProductTextView.setText(context.getString(R.string.cheapest_price) + " " + cheapestPrice);
        }
        viewHolderProductsPurchased.quantityProductTextView.setText(String.valueOf(databaseHelper.getShoppingListProductPurchasedByShoppingListIdAndProductId(shoppingList.getShoppingListId(), product.getProductID()).getShoppingListProductPurchasedQuantity()) + " Uds./Kgs.");

        return layout;
    }

    public void updateList(List<Product> lstItem) {
        products.clear();
        products.addAll(lstItem);
        this.notifyDataSetChanged();
    }
}
