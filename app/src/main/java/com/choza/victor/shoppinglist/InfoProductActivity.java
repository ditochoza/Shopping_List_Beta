package com.choza.victor.shoppinglist;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.Purchase;
import com.choza.victor.shoppinglist.model.Supermarket;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class InfoProductActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView nameText, lastUpdateText, pricesText;
    int productID;
    FabSpeedDial fabSpeedDial;
    private DatabaseHelper databaseHelper;
    private boolean dataChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_info_product);

        dataChanged = false;

        toolbar = (Toolbar) findViewById(R.id.toolbarInfo);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        nameText = (TextView) findViewById(R.id.productNameTextView);
        lastUpdateText = (TextView) findViewById(R.id.productLastPurchaseTextView);
        pricesText = (TextView) findViewById(R.id.productPricesTextView);
        fabSpeedDial = (FabSpeedDial) findViewById(R.id.fabButtonProductInfo);

        Intent intentData = getIntent();
        if (intentData != null) {
            productID = intentData.getIntExtra("productID", 0);
        }

        databaseHelper = new DatabaseHelper(this);

        setData();
        setListeners();

        setTitle("Product");
    }

    @Override
    public void onBackPressed() {
        Intent resInfo = new Intent();
        if (dataChanged){
            resInfo.putExtra("activity", "Info");
            setResult(RESULT_OK, resInfo);
        }

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent resInfo = new Intent();
                if (dataChanged){
                    resInfo.putExtra("activity", "Info");
                    setResult(RESULT_OK, resInfo);
                }

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateFields() {
        nameText.setText(databaseHelper.getProductById(productID).getProductName());
        pricesText.setText(databaseHelper.getListPricesWithSupermarketByProductId(productID));
    }

    private void setListeners() {
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.fab_menu_info_product_edit) {
                    //Se crea el cuadro de dialogo y se le pone un título
                    final AlertDialog.Builder builder = new AlertDialog.Builder(InfoProductActivity.this);
                    builder.setTitle(R.string.edit_product_name);

                    //Se usa el inflate con la vista que hemos creado
                    View viewInflated = LayoutInflater.from(InfoProductActivity.this).inflate(R.layout.dialog_edit_product, null, false);

                    //Se configura el TextInput
                    final TextInputEditText inputName = (TextInputEditText) viewInflated.findViewById(R.id.dialog_edit_product_name);

                    //Se le administra la vista creada al dialog
                    builder.setView(viewInflated);

                    //Se configura el listener del botón aceptar (Actualizar nombre producto)
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProduct();
                            dataChanged = true;
                            dialog.dismiss();
                        }

                        private void updateProduct() {
                            //Se comprueba si el campo de texto está vacío y si el nombre ya existe
                            if (!String.valueOf(inputName.getText()).isEmpty()) {
                                boolean productExtists = false;
                                List<Product> products = databaseHelper.getListProducts();
                                for (int i = 0; i < products.size(); i++) {
                                    if (products.get(i).getProductName().equals(String.valueOf(inputName.getText()))) {
                                        productExtists = true;
                                    }
                                }

                                if (!productExtists) {
                                    databaseHelper.updateProduct(String.valueOf(inputName.getText()), productID);
                                    updateFields();
                                    Toast.makeText(getApplicationContext(), R.string.saving, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), R.string.name_unique, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.name_empty, Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                    //Se configura el listener del botón cancelar
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                    return true;
                } else if (menuItem.getItemId() == R.id.fab_menu_info_product_add_price) {
                    //Se crea el cuadro de dialogo y se le pone un título
                    final AlertDialog.Builder builder = new AlertDialog.Builder(InfoProductActivity.this);
                    builder.setTitle(R.string.new_price);

                    //Se usa el inflate con la vista que hemos creado
                    View viewInflated = LayoutInflater.from(InfoProductActivity.this).inflate(R.layout.dialog_add_price, null, false);

                    //Se configuran los TextInputs
                    final AutoCompleteTextView inputName = (AutoCompleteTextView) viewInflated.findViewById(R.id.dialog_add_price_name);
                    final TextInputEditText inputPrice = (TextInputEditText) viewInflated.findViewById(R.id.dialog_add_price_price);

                    //Se cargan las opciones del AutoCompleteTextView
                    String[] supermarketsString = databaseHelper.getListSupermarketNames().toArray(new String[0]);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(InfoProductActivity.this, android.R.layout.simple_list_item_1, supermarketsString);
                    inputName.setAdapter(adapter);

                    //Se le administra la vista creada al dialog
                    builder.setView(viewInflated);

                    //Se configura el listener del botón aceptar (Añadir precio)
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            addPrice();
                            dataChanged = true;
                            dialog.dismiss();
                        }

                        private void addPrice() {

                            //Se comprueba que los campos de texto no estén vacíos
                            //Si no existe el supermercado se crea
                            if (!String.valueOf(inputName.getText()).isEmpty()) {
                                boolean productExtists = false;
                                List<Supermarket> supermarkets = databaseHelper.getListSupermarkets();
                                for (int i = 0; i < supermarkets.size(); i++) {
                                    if (supermarkets.get(i).getSupermarketName().toLowerCase().equals(String.valueOf(inputName.getText()).toLowerCase())) {
                                        productExtists = true;
                                    }
                                }

                                if (!productExtists) {
                                    @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    String currentDate = dateFormat.format(Calendar.getInstance().getTime());

                                    databaseHelper.addSupermarket(String.valueOf(inputName.getText()));
                                    databaseHelper.addPrice(databaseHelper.getSupermarketIdBySupermarketName(String.valueOf(inputName.getText())), Double.parseDouble(String.valueOf(inputPrice.getText())), currentDate, productID);
                                    databaseHelper.updateProductCheapestPrice(productID);
                                    updateFields();
                                } else {
                                    productExtists = false;
                                    for (int i = 0; i < supermarkets.size(); i++) {
                                        if (supermarkets.get(i).getSupermarketName().equals(String.valueOf(inputName.getText()))) {
                                            productExtists = true;
                                        }
                                    }

                                    if (productExtists) {
                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                        String currentDate = dateFormat.format(Calendar.getInstance().getTime());

                                        databaseHelper.addPrice(databaseHelper.getSupermarketIdBySupermarketName(String.valueOf(inputName.getText())), Double.parseDouble(String.valueOf(inputPrice.getText())), currentDate, productID);
                                        databaseHelper.updateProductCheapestPrice(productID);
                                        setData();
                                        Toast.makeText(getApplicationContext(), R.string.saving, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), R.string.name_written_incorrectly, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.name_price_empty, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    //Se configura el listener del botón cancelar
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                    return true;
                } else if (menuItem.getItemId() == R.id.fab_menu_info_product_delete) {
                    AlertDialog.Builder dialogDelete = new AlertDialog.Builder(InfoProductActivity.this);

                    dialogDelete.setTitle(R.string.delete_product);
                    dialogDelete.setMessage(R.string.confirmation_delete_product);
                    dialogDelete.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            databaseHelper.deleteProductFromShoppingListByProductId(productID);
                            databaseHelper.deteleProductById(productID);

                            Intent resInfo = new Intent();
                            resInfo.putExtra("activity", "Info");
                            setResult(RESULT_OK, resInfo);

                            finish();
                            Toast.makeText(getApplicationContext(), R.string.deleting, Toast.LENGTH_SHORT).show();
                        }
                    });


                    dialogDelete.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialogDelete.show();
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    private void setData() {
        nameText.setText(databaseHelper.getProductById(productID).getProductName());

        String lastDate = databaseHelper.getProductById(productID).getProductLastPurchase();
        if (lastDate != null) {
            lastUpdateText.setText(lastDate);
        }
        pricesText.setText(databaseHelper.getListPricesWithSupermarketByProductId(productID));
    }

}
