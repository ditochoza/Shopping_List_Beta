package com.choza.victor.shoppinglist.model;

/**
 * Created by Victor on 04/03/2018.
 */

public class ShoppingListProduct {
    private int shoppingListId;
    private int productId;
    private int shoppingListProductQuantity;

    public ShoppingListProduct(int shoppingListId, int productId, int shoppingListProductQuantity) {
        this.shoppingListId = shoppingListId;
        this.productId = productId;
        this.shoppingListProductQuantity = shoppingListProductQuantity;
    }

    public int getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(int shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShoppingListProductQuantity() {
        return shoppingListProductQuantity;
    }

    public void setShoppinglistProductQuantity(int shoppingListProductQuantity) {
        this.shoppingListProductQuantity = shoppingListProductQuantity;
    }
}
