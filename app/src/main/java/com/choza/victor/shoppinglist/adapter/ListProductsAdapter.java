package com.choza.victor.shoppinglist.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.choza.victor.shoppinglist.R;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.Purchase;

import java.util.List;

/**
 * Created by Victor on 03/03/2018.
 */

public class ListProductsAdapter extends BaseAdapter {
    private Context context;
    private List<Product> products;
    private LayoutInflater inflater;
    private DatabaseHelper databaseHelper;

    public ListProductsAdapter(Context context, List<Product> products) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.products = products;
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getProductID();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View layout = convertView;
        ViewHolderProducts viewHolderProducts;

        if (layout == null) {
            layout = inflater.inflate(R.layout.item_listview_products, parent, false);
            viewHolderProducts = new ViewHolderProducts(layout);
            layout.setTag(viewHolderProducts);
        } else {
            viewHolderProducts = (ViewHolderProducts) layout.getTag();
        }

        viewHolderProducts.nameProductTextView.setText(products.get(position).getProductName());

        String cheapestPrice = products.get(position).getProductCheapestPrice();
        if (cheapestPrice == null){
            viewHolderProducts.cheapestPriceTextView.setText("");
        }else {
            viewHolderProducts.cheapestPriceTextView.setText(context.getString(R.string.cheapest_price) + " " + cheapestPrice);
        }

        String lastDate = products.get(position).getProductLastPurchase();
        if (lastDate == null) {
            viewHolderProducts.lastPurchaseTextView.setText("");
        } else {
            viewHolderProducts.lastPurchaseTextView.setText(context.getString(R.string.last_purchase) + " " + lastDate);
        }

        return layout;
    }

    public void updateList(List<Product> lstItem) {
        products.clear();
        products.addAll(lstItem);
        this.notifyDataSetChanged();
    }
}
