package com.choza.victor.shoppinglist.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.choza.victor.shoppinglist.model.Price;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.Purchase;
import com.choza.victor.shoppinglist.model.ShoppingList;
import com.choza.victor.shoppinglist.model.ShoppingListProduct;
import com.choza.victor.shoppinglist.model.ShoppingListProductPurchased;
import com.choza.victor.shoppinglist.model.Supermarket;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Victor on 03/03/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "shopping_list.sqlite";
    public static final String DBLOCATION = "/data/data/com.choza.victor.shoppinglist/databases/";
    private Context context;
    private SQLiteDatabase dataBase;

    public DatabaseHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = context.getDatabasePath(DBNAME).getPath();
        if (dataBase != null && dataBase.isOpen()) {
            return;
        }
        dataBase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if (dataBase != null) {
            dataBase.close();
        }
    }

    public List<Product> getListProducts() {
        Product product = null;
        List<Product> products = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PRODUCT", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            product = new Product(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            products.add(product);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return products;
    }

    public List<ShoppingList> getListShoppingLists() {
        ShoppingList shoppingList = null;
        List<ShoppingList> shoppingLists = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SHOPPINGLIST", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            shoppingList = new ShoppingList(cursor.getInt(0), cursor.getString(1));
            shoppingLists.add(shoppingList);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return shoppingLists;
    }

    public List<Supermarket> getListSupermarkets() {
        Supermarket supermarket = null;
        List<Supermarket> supermarkets = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SUPERMARKET", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            supermarket = new Supermarket(cursor.getInt(0), cursor.getString(1));
            supermarkets.add(supermarket);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return supermarkets;
    }

    public List<Purchase> getListPurchases() {
        Purchase purchase = null;
        List<Purchase> purchases = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PURCHASE", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            purchase = new Purchase(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getInt(3));
            purchases.add(purchase);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return purchases;
    }

    public int getPurchasesCount(){
        Purchase purchase = null;
        List<Purchase> purchases = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PURCHASE", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            purchase = new Purchase(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getInt(3));
            purchases.add(purchase);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return purchases.size();
    }

    public List<String> getListSupermarketNames() {
        List<Supermarket> supermarkets = getListSupermarkets();
        ArrayList<String> supermarketNames = new ArrayList<>();

        for (int i = 0; i < supermarkets.size(); i++) {
            supermarketNames.add(supermarkets.get(i).getSupermarketName());
        }

        return supermarketNames;
    }

    public List<String> getListProductNames() {
        List<Product> products = getListProducts();
        ArrayList<String> productNames = new ArrayList<>();

        for (int i = 0; i < products.size(); i++) {
            productNames.add(products.get(i).getProductName());
        }

        return productNames;
    }

    public Product getProductById(int id) {
        Product product = null;
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PRODUCT WHERE PRODUCTID = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        product = new Product(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));

        cursor.close();
        closeDatabase();
        return product;
    }

    public ShoppingList getShoppingListById(int id) {
        ShoppingList shoppingList = null;
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SHOPPINGLIST WHERE SHOPPINGLISTID = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        shoppingList = new ShoppingList(cursor.getInt(0), cursor.getString(1));

        cursor.close();
        closeDatabase();
        return shoppingList;
    }

    public Product getProductByName(String name) {
        Product product = null;
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PRODUCT WHERE PRODUCTNAME = ?", new String[]{String.valueOf(name)});
        cursor.moveToFirst();
        product = new Product(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));

        cursor.close();
        closeDatabase();
        return product;
    }

    public Supermarket getSupermarketById(int id) {
        Supermarket supermarket = null;
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SUPERMARKET WHERE SUPERMARKETID = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        supermarket = new Supermarket(cursor.getInt(0), cursor.getString(1));

        cursor.close();
        closeDatabase();
        return supermarket;
    }

    public ShoppingListProduct getShoppingListProductByProductIdAndShoppingListId(int productID, int shoppingListId) {
        ShoppingListProduct shoppingListProduct = null;
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SHOPPINGLISTPRODUCT WHERE PRODUCT =? AND SHOPPINGLIST =?", new String[]{String.valueOf(productID), String.valueOf(shoppingListId)});
        cursor.moveToFirst();
        shoppingListProduct = new ShoppingListProduct(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2));

        cursor.close();
        closeDatabase();
        return shoppingListProduct;
    }

    public List<Price> getListPricesByProductId(int id) {
        Price price = null;
        List<Price> prices = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PRICE WHERE PRODUCT = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            price = new Price(cursor.getInt(0), cursor.getDouble(1), cursor.getString(2), cursor.getInt(3), cursor.getInt(4));
            prices.add(price);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return prices;
    }

    public List<Product> getListProductsByShoppingListId(int id){
        Product product = null;
        ShoppingListProduct shoppingListProduct = null;
        List<Product> products = new ArrayList<>();
        List<ShoppingListProduct> shoppingListProducts = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SHOPPINGLISTPRODUCT WHERE SHOPPINGLIST = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            shoppingListProduct = new ShoppingListProduct(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2));
            shoppingListProducts.add(shoppingListProduct);
            cursor.moveToNext();
        }

        for (int i = 0; i < shoppingListProducts.size(); i++){
            product = getProductById(shoppingListProducts.get(i).getProductId());
            products.add(product);
        }

        cursor.close();
        closeDatabase();
        return products;
    }

    public List<Product> getListProductsPurchasedByShoppingList(int id){
        Product product = null;
        ShoppingListProductPurchased shoppingListProductPurchased = null;
        List<Product> products = new ArrayList<>();
        List<ShoppingListProductPurchased> shoppingListProductsPurchased = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SHOPPINGLISTPRODUCTPURCHASED WHERE SHOPPINGLIST = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            shoppingListProductPurchased = new ShoppingListProductPurchased(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5));
            shoppingListProductsPurchased.add(shoppingListProductPurchased);
            cursor.moveToNext();
        }

        for (int i = 0; i < shoppingListProductsPurchased.size(); i++){
            product = getProductById(shoppingListProductsPurchased.get(i).getProductId());
            products.add(product);
        }

        cursor.close();
        closeDatabase();
        return products;
    }

    public ShoppingListProductPurchased getShoppingListProductPurchasedByShoppingListIdAndProductId(int shoppingListId, int productId){
        ShoppingListProductPurchased shoppingListProductPurchased = null;
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM SHOPPINGLISTPRODUCTPURCHASED WHERE SHOPPINGLIST =? AND PRODUCT =?", new String[]{String.valueOf(shoppingListId), String.valueOf(productId)});
        cursor.moveToFirst();
        shoppingListProductPurchased = new ShoppingListProductPurchased(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5));

        cursor.close();
        closeDatabase();
        return shoppingListProductPurchased;
    }

    public List<Purchase> getListPurchasesByProductId(int id) {
        Purchase purchase = null;
        List<Purchase> purchases = new ArrayList<>();
        openDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT * FROM PURCHASE WHERE PRODUCT = ?", new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            purchase = new Purchase(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getInt(3));
            purchases.add(purchase);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return purchases;
    }

    public int getSupermarketIdBySupermarketName(String name) {
        List<Supermarket> supermarkets = getListSupermarkets();
        int id = 0;

        for (int i = 0; i < supermarkets.size(); i++) {
            if (supermarkets.get(i).getSupermarketName().toLowerCase().equals(name.toLowerCase())) {
                id = supermarkets.get(i).getSupermarketID();
            }
        }
        return id;
    }

    public Purchase calculateLastPurchaseDateByProductId(int id) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<Purchase> purchases = getListPurchasesByProductId(id);
        ArrayList<Calendar> datesPurchase = new ArrayList<>();
        Calendar lastDate = Calendar.getInstance();
        Purchase lastPurchase = null;

        if (purchases.size() == 0) {
            lastPurchase = null;
        } else {
            for (int i = 0; i < purchases.size(); i++) {
                try {
                    Calendar datePurchase = Calendar.getInstance();
                    datePurchase.setTime(dateFormat.parse(purchases.get(i).getPurchaseDate()));
                    datesPurchase.add(datePurchase);
                } catch (ParseException ignored) {
                }
            }

            for (int i = 0; i < purchases.size(); ++i) {
                try {
                    if (i == 0) {
                        lastDate.setTime(dateFormat.parse("01/01/2001"));
                    }
                    if (datesPurchase.get(i).after(lastDate)) {
                        lastDate = datesPurchase.get(i);
                        lastPurchase = purchases.get(i);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return lastPurchase;
    }

    public String calculateCheapestPriceWithSupermarketByProductId(int id) {
        Price cheapestPrice = null;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            List<Price> prices = getListPricesByProductId(id);
            if (prices.isEmpty()) {
                return "";
            }
            ArrayList<Supermarket> supermarkets = new ArrayList<>();
            ArrayList<Price> latestPricesEachSupermarket = new ArrayList<>();

            //Recogo todos los supermercados que hay en los precios;
            boolean duplicatedSupermarket;
            for (int i = 0; i < prices.size(); i++) {
                duplicatedSupermarket = false;
                for (int j = 0; j < supermarkets.size(); j++) {
                    if (supermarkets.get(j).getSupermarketID() == getSupermarketById(prices.get(i).getSupermarketID()).getSupermarketID()) {
                        duplicatedSupermarket = true;
                    }
                }
                if (!duplicatedSupermarket) {
                    supermarkets.add(getSupermarketById(prices.get(i).getSupermarketID()));
                }
            }

            //Se crea un array de los precios más actuales de cada supermercado
            for (int i = 0; i < supermarkets.size(); i++) {
                Price latestPriceEachSupermarket = null;
                ArrayList<Price> pricesEachSupermarket = new ArrayList<>();
                for (int j = 0; j < prices.size(); j++) {
                    if (supermarkets.get(i).getSupermarketID() == getSupermarketById(prices.get(j).getSupermarketID()).getSupermarketID()) {
                        pricesEachSupermarket.add(prices.get(j));
                    }
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateFormat.parse("01/01/2001"));
                for (int j = 0; j < pricesEachSupermarket.size(); j++) {
                    Calendar tempCalendar = Calendar.getInstance();
                    tempCalendar.setTime(dateFormat.parse(pricesEachSupermarket.get(j).getPriceDate()));
                    if (tempCalendar.after(calendar)) {
                        latestPriceEachSupermarket = pricesEachSupermarket.get(j);
                    }
                }
                latestPricesEachSupermarket.add(latestPriceEachSupermarket);

            }

            //Se saca el precio más bajo
            Double cheapestPriceTag = 10000000.0;
            for (int i = 0; i < latestPricesEachSupermarket.size(); i++) {
                if (latestPricesEachSupermarket.get(i).getPriceTag() < cheapestPriceTag) {
                    cheapestPrice = latestPricesEachSupermarket.get(i);
                    cheapestPriceTag = cheapestPrice.getPriceTag();
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return cheapestPrice.getPriceTag() + " (" + getSupermarketById(cheapestPrice.getSupermarketID()).getSupermarketName() + ")";
    }

    public String getListPricesWithSupermarketByProductId(int id) {
        List<Price> prices = getListPricesByProductId(id);
        String price = "";

        for (int i = 0; i < prices.size(); i++) {
            if (i != 0) {
                price += "\n";
            }
            price += getSupermarketById(prices.get(i).getSupermarketID()).getSupermarketName() + " - ";
            price += prices.get(i).getPriceDate() + ": ";
            price += prices.get(i).getPriceTag() + "€";
        }

        return price;
    }

    public long updateProduct(String productName, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("PRODUCTNAME", productName);
        String[] whereArgs = {Integer.toString(id)};
        openDatabase();
        long returnValue = dataBase.update("PRODUCT", contentValues, "PRODUCTID=?", whereArgs);
        closeDatabase();
        return returnValue;
    }

    public long updateProductLastPurchase(int id) {
        ContentValues contentValues = new ContentValues();
        if (calculateLastPurchaseDateByProductId(id) == null){
            Log.w("2", "updateProductLastPurchase: ");
            contentValues.putNull("PRODUCTLASTPURCHASE");
        }else {
            contentValues.put("PRODUCTLASTPURCHASE", calculateLastPurchaseDateByProductId(id).getPurchaseDate());
        }
        String[] whereArgs = {Integer.toString(id)};
        openDatabase();
        long returnValue = dataBase.update("PRODUCT", contentValues, "PRODUCTID=?", whereArgs);
        closeDatabase();
        return returnValue;
    }

    public long updateProductCheapestPrice(int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("PRODUCTCHEAPESTPRICE", calculateCheapestPriceWithSupermarketByProductId(id));
        String[] whereArgs = {Integer.toString(id)};
        openDatabase();
        long returnValue = dataBase.update("PRODUCT", contentValues, "PRODUCTID=?", whereArgs);
        closeDatabase();
        return returnValue;
    }

    public long updateShoppingList(String shoppingListName, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("SHOPPINGLISTNAME", shoppingListName);
        String[] whereArgs = {Integer.toString(id)};
        openDatabase();
        long returnValue = dataBase.update("SHOPPINGLIST", contentValues, "SHOPPINGLISTID=?", whereArgs);
        closeDatabase();
        return returnValue;
    }

    public long addPrice(int supermarketId, double priceTag, String priceDate, int productId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("SUPERMARKET", supermarketId);
        contentValues.put("PRICETAG", priceTag);
        contentValues.put("PRICEDATE", priceDate);
        contentValues.put("PRODUCT", productId);
        openDatabase();
        long returnValue = dataBase.insert("PRICE", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public long addSupermarket(String supermarketName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("SUPERMARKETNAME", supermarketName);
        openDatabase();
        long returnValue = dataBase.insert("SUPERMARKET", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public long addProduct(String productName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("PRODUCTNAME", productName);
        openDatabase();
        long returnValue = dataBase.insert("PRODUCT", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public long addShoppingList(String shoppingListName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("SHOPPINGLISTNAME", shoppingListName);
        openDatabase();
        long returnValue = dataBase.insert("SHOPPINGLIST", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public long addPurchase(int productId, String purchaseDate, int purchaseQuantity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("PRODUCT", productId);
        contentValues.put("PURCHASEDATE", purchaseDate);
        contentValues.put("PURCHASEQUANTITY", purchaseQuantity);
        openDatabase();
        long returnValue = dataBase.insert("PURCHASE", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public long addShoppingListProduct(int shoppingListID, int productID , int quantity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("SHOPPINGLIST", shoppingListID);
        contentValues.put("PRODUCT", productID);
        contentValues.put("SHOPPINGLISTPRODUCTQUANTITY", quantity);
        openDatabase();
        long returnValue = dataBase.insert("SHOPPINGLISTPRODUCT", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public long addShoppingListProductPurchased(int shoppingListID, int suggestedShoppingListID, int productID, int purchaseID, int quantity){
        ContentValues contentValues = new ContentValues();
        if (shoppingListID != 0){
            contentValues.put("SHOPPINGLIST", shoppingListID);
        }else if (suggestedShoppingListID != 0){
            contentValues.put("SUGGESTEDSHOPPINGLIST", suggestedShoppingListID);
        }
        contentValues.put("PRODUCT", productID);
        contentValues.put("PURCHASE", purchaseID);
        contentValues.put("SHOPPINGLISTPRODUCTPURCHASEDQUANTITY", quantity);
        openDatabase();
        long returnValue = dataBase.insert("SHOPPINGLISTPRODUCTPURCHASED", null, contentValues);
        closeDatabase();
        return returnValue;
    }

    public boolean deleteProductFromShoppingListByProductId(int id) {
        openDatabase();
        int result = dataBase.delete("SHOPPINGLISTPRODUCT",  "PRODUCT =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }

    public boolean deteleProductById(int id) {
        openDatabase();
        int result = dataBase.delete("PRODUCT",  "PRODUCTID =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }

    public boolean deleteShoppingListById(int id) {
        openDatabase();
        int result = dataBase.delete("SHOPPINGLIST",  "SHOPPINGLISTID =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }

    public boolean deleteProductFromShoppingListByProductIdAndShoppingListId(int productId, int shoppingListId){
        openDatabase();
        int result = dataBase.delete("SHOPPINGLISTPRODUCT",  "PRODUCT =? AND SHOPPINGLIST =?", new String[]{String.valueOf(productId), String.valueOf(shoppingListId)});
        closeDatabase();
        return result !=0;
    }

    public boolean deleteShoppingListProductPurchasedByShoppingListId(int id){
        openDatabase();
        int result = dataBase.delete("SHOPPINGLISTPRODUCTPURCHASED",  "SHOPPINGLIST =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }

    public boolean deleteShoppingListProductByShoppingListId(int id){
        openDatabase();
        int result = dataBase.delete("SHOPPINGLISTPRODUCT",  "SHOPPINGLIST =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }

    public boolean deleteProductFromShoppingListPurchasedByProductIdAndShoppingListId(int productID, int shoppingListID) {
        openDatabase();
        int result = dataBase.delete("SHOPPINGLISTPRODUCTPURCHASED",  "PRODUCT =? AND SHOPPINGLIST =?", new String[]{String.valueOf(productID), String.valueOf(shoppingListID)});
        closeDatabase();
        return result !=0;
    }

    public boolean deletePurchaseById(int id) {
        openDatabase();
        int result = dataBase.delete("PURCHASE",  "PURCHASEID =?", new String[]{String.valueOf(id)});
        closeDatabase();
        return result !=0;
    }
}
