package com.choza.victor.shoppinglist.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.choza.victor.shoppinglist.InfoProductShoppingListActivity;
import com.choza.victor.shoppinglist.InfoShoppingListActivity;
import com.choza.victor.shoppinglist.R;
import com.choza.victor.shoppinglist.database.DatabaseHelper;
import com.choza.victor.shoppinglist.model.Product;
import com.choza.victor.shoppinglist.model.ShoppingList;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Victor on 04/03/2018.
 */

public class ListProductsShoppingListAdapter extends BaseAdapter {
    private Context context;
    private List<Product> products;
    private LayoutInflater inflater;
    private DatabaseHelper databaseHelper;
    private ShoppingList shoppingList;
    private int INFO_PRODUCT_SHOPPING_LIST_ACTIVITY = 7;
    private InfoShoppingListActivity infoShoppingListActivity;

    public ListProductsShoppingListAdapter(Context context, List<Product> products, ShoppingList shoppingList, InfoShoppingListActivity infoShoppingListActivity) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.products = products;
        this.shoppingList = shoppingList;
        this.infoShoppingListActivity = infoShoppingListActivity;
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getProductID();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {

        View layout = convertView;
        final ViewHolderProductsShoppingList viewHolderProductsShoppingList;

        if (layout == null) {
            layout = inflater.inflate(R.layout.item_listview_products_shopping_list, parent, false);
            viewHolderProductsShoppingList = new ViewHolderProductsShoppingList(layout);
            layout.setTag(viewHolderProductsShoppingList);
        } else {
            viewHolderProductsShoppingList = (ViewHolderProductsShoppingList) layout.getTag();
        }

        viewHolderProductsShoppingList.nameProductTextView.setText(products.get(position).getProductName());

        String cheapestPrice = databaseHelper.getProductById(products.get(position).getProductID()).getProductCheapestPrice();
        if (cheapestPrice == null) {
            viewHolderProductsShoppingList.priceProductTextView.setText("");
        } else {
            viewHolderProductsShoppingList.priceProductTextView.setText(context.getString(R.string.cheapest_price) + " " + cheapestPrice);
        }

        viewHolderProductsShoppingList.quantityProductTextView.setText(String.valueOf(databaseHelper.getShoppingListProductByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingList.getShoppingListId()).getShoppingListProductQuantity()) + " Uds./Kgs.");

        viewHolderProductsShoppingList.checkProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String currentDate = dateFormat.format(Calendar.getInstance().getTime());

                long id = databaseHelper.addPurchase(products.get(position).getProductID(), currentDate, databaseHelper.getShoppingListProductByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingList.getShoppingListId()).getShoppingListProductQuantity());
                databaseHelper.addShoppingListProductPurchased(shoppingList.getShoppingListId(), 0, products.get(position).getProductID(), Integer.parseInt(String.valueOf(id)), databaseHelper.getShoppingListProductByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingList.getShoppingListId()).getShoppingListProductQuantity());
                databaseHelper.updateProductLastPurchase(products.get(position).getProductID());
                databaseHelper.deleteProductFromShoppingListByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingList.getShoppingListId());

                Toast.makeText(context, R.string.sending_checked_products, Toast.LENGTH_SHORT).show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        products.remove(position);
                        infoShoppingListActivity.dataChanged = true;
                        notifyDataSetChanged();
                    }
                }, 400);
            }
        });

        viewHolderProductsShoppingList.deleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseHelper.deleteProductFromShoppingListByProductIdAndShoppingListId(products.get(position).getProductID(), shoppingList.getShoppingListId());

                Toast.makeText(context, R.string.deleting, Toast.LENGTH_SHORT).show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        products.remove(position);
                        infoShoppingListActivity.dataChanged = true;
                        notifyDataSetChanged();
                    }
                }, 400);
            }


        });

        viewHolderProductsShoppingList.relativeLayout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Intent intentInfo = new Intent(context, InfoProductShoppingListActivity.class);
                intentInfo.putExtra("productID", products.get(position).getProductID());
                intentInfo.putExtra("shoppingListID", shoppingList.getShoppingListId());

                ((InfoShoppingListActivity) context).startActivityForResult(intentInfo, INFO_PRODUCT_SHOPPING_LIST_ACTIVITY);
            }
        });

        return layout;
    }

    public void updateList(List<Product> lstItem) {
        products.clear();
        products.addAll(lstItem);
        this.notifyDataSetChanged();
    }
}
