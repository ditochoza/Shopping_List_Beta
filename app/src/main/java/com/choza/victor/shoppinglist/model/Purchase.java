package com.choza.victor.shoppinglist.model;

/**
 * Created by Victor on 03/03/2018.
 */

public class Purchase {
    private int purchaseId;
    private int productId;
    private String purchaseDate;
    private int purchaseQuantity;

    public Purchase(int productID, int productId, String purchaseDate, int purchaseQuantity) {
        this.purchaseId = productID;
        this.productId = productId;
        this.purchaseDate = purchaseDate;
        this.purchaseQuantity = purchaseQuantity;

    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public int getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void setPurchaseQuantity(int purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }
}
