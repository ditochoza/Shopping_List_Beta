package com.choza.victor.shoppinglist.adapter;

import android.view.View;
import android.widget.TextView;

import com.choza.victor.shoppinglist.R;

/**
 * Created by Victor on 03/03/2018.
 */

public class ViewHolderProducts {
    public TextView nameProductTextView;
    public TextView cheapestPriceTextView;
    public TextView lastPurchaseTextView;

    ViewHolderProducts(View view){
        nameProductTextView = (TextView) view.findViewById(R.id.name_product);
        cheapestPriceTextView = (TextView) view.findViewById(R.id.price_product);
        lastPurchaseTextView = (TextView) view.findViewById(R.id.last_update_product);

    }
}
