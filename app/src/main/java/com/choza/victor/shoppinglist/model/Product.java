package com.choza.victor.shoppinglist.model;

/**
 * Created by Victor on 03/03/2018.
 */

public class Product {
    private int productID;
    private String productName, productCheapestPrice, productLastPurchase;

    public Product(int productID, String productName, String productCheapestPrice, String productLastPurchase) {
        this.productID = productID;
        this.productName = productName;
        this.productCheapestPrice = productCheapestPrice;
        this.productLastPurchase = productLastPurchase;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCheapestPrice() {
        return productCheapestPrice;
    }

    public void setProductCheapestPrice(String productCheapestPrice) {
        this.productCheapestPrice = productCheapestPrice;
    }

    public String getProductLastPurchase() {
        return productLastPurchase;
    }

    public void setProductLastPurchase(String productLastPurchase) {
        this.productLastPurchase = productLastPurchase;
    }
}
