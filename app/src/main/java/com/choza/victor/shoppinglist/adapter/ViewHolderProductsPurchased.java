package com.choza.victor.shoppinglist.adapter;

import android.view.View;
import android.widget.TextView;

import com.choza.victor.shoppinglist.R;

/**
 * Created by Victor on 09/03/2018.
 */

public class ViewHolderProductsPurchased {
    public TextView nameProductTextView, priceProductTextView, quantityProductTextView;

    ViewHolderProductsPurchased(View view){
        nameProductTextView = (TextView) view.findViewById(R.id.item_listview_products_purchased_name_textView);
        priceProductTextView = (TextView) view.findViewById(R.id.item_listview_products_purchased_price_textView);
        quantityProductTextView = (TextView) view.findViewById(R.id.item_listview_products_purchased_quantity_textView);

    }
}
